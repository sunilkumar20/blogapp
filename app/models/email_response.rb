class EmailResponse < ActiveRecord::Base
	enum response_type: [ :bounce, :complaint ]
end
