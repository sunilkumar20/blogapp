# config valid only for current version of Capistrano
lock '3.4.1'

set :application, 'blogapp'
set :scm, :git
set :repo_url, 'git@gitlab.com:sunilascratech/blogapp.git'
set :scm_passphrase, ""
# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('database.yml', 'secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }
set :bundle_flags, '--deployment --quiet'
# Default value for keep_releases is 5
# set :keep_releases, 5
set :keep_releases, 1
# set :use_sudo, true
# set :ssh_options, { forward_agent: true, user: fetch(:user), keys: %w(/home/amol/.ssh/id_rsa) }

set :bundle_gemfile, -> { release_path.join('Gemfile') }
set :bundle_dir, -> { shared_path.join('bundle') }
set :bundle_flags, ''
set :bundle_without, %w{test development}.join(' ')
set :bundle_binstubs, -> { shared_path.join('bin') }
set :bundle_roles, :all

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
    on roles(:db) do 
      execute :rake, "db:migrate RAILS_ENV=production"
    end
  end
 #  before "deploy:assets:precompile", "deploy:bundle_install"
	# desc "Bundle install for RVMs sake"
	# task :bundle_install do
	#   on roles(:app) do
	#     execute "bundle install"
	#   end
	# end

end

# set :role, :db, 'blogappproduction.cjq40cfmyliq.us-west-2.rds.amazonaws.com', :primary => true
